(function( $ ){

  var methods = {
     init : function( options ) {

       return this.each(function(){

         $(this).append('<input type="text" class="textTag" >&nbsp;');
         $(this).append('<input type="button" class="buttonTag" value="Ekle">');
         $(this).append('<div class="listTag"> </div>');
         
         //$(this).children(":button").first().button();

         $(this).css("clear","both");
         
         $(this).children(".textTag").keydown(function(event){
		if (event.keyCode == 32) return false;
		if (event.keyCode == 13) $(this).parent().children(".buttonTag").click();
		});


         $(this).children(".buttonTag").click(function(){

                var objList = $(this).parent().children("div").first();
                var objText = $(this).parent().children(":input").first();

                
                if ($(objText).val().length < 3) return false;

                $(objList).prepend('<div class="tagItem"><div style="float:left; margin-right:3px; margin-top:3px;"><div class="tagSil deleteTag1"></div></div> '+$(objText).val()+'<input type="hidden" name="tagValue[]" id="tagValue[]" value="'+$(objText).val()+'"></div>');

                $(objText).val("")

                
                $(objList).find(".tagSil").hover( // uzerine gelince css duznenle
			function () {
				$(this).removeClass("deleteTag1");
				$(this).addClass("deleteTag2");
			  },
                          function () {
                                $(this).removeClass("deleteTag2");
                                $(this).addClass("deleteTag1");
                          }
                        );

                $(objList).find(".tagSil").unbind('click').click(function(){
			$(this).parent().parent().remove();
			});
                      
		})

                

       });



     },
	 clear : function(){
		 $(this).children(".listTag").find("*").remove();
		 },
     tagsCount : function (){
         return $(this).children(".listTag").children(".tagItem").length;
     }
     ,
     destroy : function( ) {

       return this.each(function(){
         $(window).unbind('.tagsBox');
       })

     },
	 add : function(data) {
		
		return this.each(function(){
         var objList = $(this).children("div").first();
		 
		 $(objList).prepend('<div class="tagItem"><div style="float:left; margin-right:3px; margin-top:3px;"><div class="tagSil deleteTag1"></div></div> '+data+' <input type="hidden" name="tagValue[]" id="tagValue[]" value="'+data+'"></div>');
			$(objList).find(".tagSil").hover( // uzerine gelince css duznenle
			function () {
				$(this).removeClass("deleteTag1");
				$(this).addClass("deleteTag2");
			  },
                          function () {
                                $(this).removeClass("deleteTag2");
                                $(this).addClass("deleteTag1");
                          }
                        );

                $(objList).find(".tagSil").unbind('click').click(function(){
			$(this).parent().parent().remove();
			});
			
       })
       
     }
  };

  $.fn.tagsBox = function( method ) {

    if ( methods[method] ) {
      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
    }

  };

})( jQuery );